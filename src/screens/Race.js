import React, {Component} from 'react';
import {View, StyleSheet, Text, Platform, Alert} from 'react-native';
import {MAPBOX_TOKEN} from 'react-native-dotenv';
import Mapbox, {
  MapView,
  Camera,
  ShapeSource,
  LineLayer,
  UserLocation,
} from '@react-native-mapbox-gl/maps';
import RoundButton from '../components/RoundButton';
import LinearGradient from 'react-native-linear-gradient';
import TimeFormat from 'hh-mm-ss';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import messaging from '@react-native-firebase/messaging';
import distance from '@turf/distance';

export default class Race extends Component {
  state = {
    centerCoordinate: [0, 0],
    zoomLevel: 12,
    /* (stopped | started ) */
    status: 'stopped',
    polyline: {
      type: 'FeatureCollection',
      features: [],
    },
    duration: 0,
    dist: 0,
    position: {
      coords: {
        heading: 0,
        altitude: 0,
        longitude: null,
        latitude: null,
        speed: 0,
      },
      timestamp: Date.now(),
    },
  };

  timer = null;

  layerStyle = {
    lineJoin: 'round',
    lineColor: [
      'interpolate',
      ['linear'],
      ['get', 'speed'],
      0,
      '#1abc9c',
      5,
      '#2ecc71',
      10,
      '#3498db',
      15,
      '#9b59b6',
      20,
      '#d35400',
      25,
      '#c0392b',
    ],
    lineWidth: 3,
  };

  docRef = '';

  styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    map: {
      flex: 1,
    },
    buttonGroup: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: 60,
      position: 'absolute',
      bottom: 60,
    },
    overlay: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-evenly',
      alignItems: 'center',
      width: '100%',
      height: 100,
      position: 'absolute',
      top: 0,
    },
    infoGroup: {
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },
    info: {
      color: '#ffffff',
      fontFamily: 'Open Sans',
      fontSize: 20,
    },
  });

  async componentDidMount() {
    Mapbox.setAccessToken(MAPBOX_TOKEN);
    this.unsubscribe = messaging().onMessage(async message => {
      Alert.alert(
        message.data.notification.title,
        message.data.notification.body,
      );
    });
    if (Platform.OS === 'android') {
      Mapbox.requestAndroidLocationPermissions()
        .then(res => console.log(res))
        .catch(err => console.log(err));
    }
  }

  componentDidUnMount() {
    this.unsubscribe();
    if (this.state.status === 'started') {
      this.onStop();
    }
  }

  async onStart() {
    console.log('start run');
    let start = Date.now();
    this.setState({status: 'started', start});
    this.docRef = await firestore()
      .collection('users')
      .doc(auth().currentUser.email)
      .collection('runs')
      .doc().path;
    console.log(this.docRef);

    await firestore()
      .doc(this.docRef)
      .set({status: 'started', start});

    clearInterval(this.timer);
    this.timer = setInterval(() => {
      this.setState({duration: Date.now() - start});
    }, 1000);
  }

  async onStop() {
    console.log('stop run');
    this.setState({
      status: 'stopped',
      duration: 0,
      dist: 0,
      polyline: {
        type: 'FeatureCollection',
        features: [],
      },
    });
    await firestore()
      .doc(this.docRef)
      .set(
        {
          status: 'stopped',
          end: Date.now(),
          distance: this.state.dist,
          duration: this.state.duration,
        },
        {merge: true},
      );
    clearInterval(this.timer);
  }

  async onUpdate(location) {
    if (this.state.status === 'started') {
      let dist = distance(
        [
          this.state.position.coords.longitude,
          this.state.position.coords.latitude,
        ],
        [location.coords.longitude, location.coords.latitude],
      );
      let feature = {
        type: 'Feature',
        geometry: {
          type: 'LineString',
          coordinates: [
            [
              this.state.position.coords.longitude,
              this.state.position.coords.latitude,
            ],
            [location.coords.longitude, location.coords.latitude],
          ],
        },
        properties: {
          altitude: location.coords.altitude,
          heading: location.coords.heading,
          speed: location.coords.speed,
        },
      };
      this.setState({
        polyline: {
          type: 'FeatureCollection',
          features: [...this.state.polyline.features, feature],
        },
        dist: this.state.dist + dist,
      });
      await firestore()
        .doc(this.docRef)
        .collection('positions')
        .add(location);
    }
    if (location) {
      this.setState({position: location});
    }
  }

  render() {
    let {
      status,
      centerCoordinate,
      zoomLevel,
      polyline,
      position,
      duration,
      dist,
    } = this.state;

    return (
      <View style={this.styles.container}>
        <MapView
          ref={c => (this._map = c)}
          style={this.styles.map}
          styleURL={Mapbox.StyleURL.Light}
          onPress={() => console.log('map')}>
          <Camera
            centerCoordinate={centerCoordinate}
            zoomLevel={zoomLevel}
            followUserLocation
            followUserMode={'normal'}
          />
          <ShapeSource id="polylineSource" shape={polyline}>
            <LineLayer id="polylineLayer" style={this.layerStyle} />
          </ShapeSource>
          <UserLocation
            visible
            minDisplacement={10}
            onUpdate={location => this.onUpdate(location)}
          />
        </MapView>
        <LinearGradient
          style={this.styles.overlay}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#27AE60', '#1ABC9C']}>
          <View style={this.styles.infoGroup}>
            <Text style={this.styles.info}>{dist.toFixed(2)} km</Text>
          </View>
          <View style={this.styles.infoGroup}>
            <Text style={this.styles.info}>
              {TimeFormat.fromS(Math.round(duration / 1000), 'hh:mm:ss')}
            </Text>
          </View>
          <View style={this.styles.infoGroup}>
            <Text style={this.styles.info}>{position.coords.speed} km/h</Text>
          </View>
        </LinearGradient>
        {status === 'stopped' && (
          <View style={this.styles.buttonGroup}>
            <RoundButton iconSize={25} size={50} icon="music-note" />
            <RoundButton
              iconSize={35}
              size={70}
              icon="play"
              onPress={e => this.onStart(e)}
            />
            <RoundButton iconSize={25} size={50} icon="settings" />
          </View>
        )}
        {status === 'started' && (
          <View style={this.styles.buttonGroup}>
            <RoundButton iconSize={25} size={50} icon="music-note" />
            <RoundButton
              iconSize={35}
              size={70}
              icon="stop"
              onPress={e => this.onStop(e)}
            />
            <RoundButton iconSize={25} size={50} icon="lock-open" />
          </View>
        )}
      </View>
    );
  }
}
