import {View, StyleSheet, TextInput, Text, Button} from 'react-native';
import React, {Component} from 'react';
import auth from '@react-native-firebase/auth';
import messaging from '@react-native-firebase/messaging';
import firestore from '@react-native-firebase/firestore';

export default class Login extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    email: '',
    password: '',
    errorMessage: null,
  };

  static navigationOptions = {
    title: 'Connexion',
    headerStyle: {
      backgroundColor: '#211d1c',
    },
    headerTitleStyle: {
      color: 'white',
      textAlign: 'center',
      alignSelf: 'center',
      flex: 1,
    },
  };

  async componentDidMount() {
    await messaging().requestPermission();
    if (auth().currentUser != null) {
      this.props.navigation.navigate('Race');
    }
    messaging().onTokenRefresh(token =>
      this.saveTokenToDatabase(token, auth().currentUser.email),
    );
  }

  handleLogin = async () => {
    const {email, password} = this.state;
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => this.props.navigation.navigate('Race'))
      .catch(error => this.setState({errorMessage: error.message}));
    messaging()
      .getToken()
      .then(token => this.saveTokenToDatabase(token, email));
  };

  saveTokenToDatabase = async (token, email) => {
    await firestore()
      .collection('users')
      .doc(email)
      .set(
        {
          token: token,
        },
        {merge: true},
      );
  };

  navigateSignUp = () => {
    this.props.navigation.navigate('SignUp');
  };

  navigateSendPassword = () => {
    this.props.navigation.navigate('ChangePassword');
  };

  render() {
    return (
      <>
        <View style={styles.globalContainer}>
          <Text style={styles.globalTitle}>Se connecter</Text>
          <View style={styles.inputContainer}>
            <Text style={styles.title}>E-mail</Text>
            <TextInput
              style={styles.input}
              onChangeText={email => this.setState({email})}
              value={this.state.email}
              autoCapitalize="none"
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.title}>Mot de passe</Text>
            <TextInput
              secureTextEntry={true}
              style={styles.input}
              onChangeText={password => this.setState({password})}
              value={this.state.password}
              autoCapitalize="none"
            />
          </View>
          <View style={styles.button}>
            <Button
              color="#27AE60"
              style={styles.button}
              title="Se Connecter"
              onPress={this.handleLogin}
            />
          </View>
          {this.state.errorMessage && (
            <Text style={styles.error}>{this.state.errorMessage}</Text>
          )}
          <Text onPress={this.navigateSendPassword} style={styles.link}>
            Mot de passe oublié ?
          </Text>
          <Text style={styles.title}>Vous n'avez pas encore de compte ?</Text>
          <Button onPress={this.navigateSignUp} title="Créer un compte" />
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  globalContainer: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#2F2E2D',
    height: '100%',
  },
  globalTitle: {
    color: 'white',
    marginTop: 20,
    fontSize: 18,
  },
  title: {
    color: 'white',
    marginBottom: 10,
    marginTop: 20,
    fontSize: 16,
  },
  link: {
    color: '#4287f5',
    marginTop: 15,
    marginBottom: 10,
    fontSize: 16,
  },
  inputContainer: {
    marginBottom: 25,
    width: 190,
  },
  input: {
    backgroundColor: 'white',
    borderRadius: 10,
  },
  button: {
    width: 150,
  },
  error: {
    width: 250,
    color: 'red',
  },
});
