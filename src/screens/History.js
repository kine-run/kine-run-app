import {View, Text} from 'react-native';
import React, {Component} from 'react';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import HistoryCard from '../components/HistoryCard';
import {FlatList} from 'react-native-gesture-handler';

export default class History extends Component {
  state = {
    history: [],
  };

  async componentDidMount() {
    this.unsusbcribe = await firestore()
      .collection('users')
      .doc(auth().currentUser.email)
      .collection('runs')
      .where('status', '==', 'stopped')
      .onSnapshot(snapshot => {
        this.setState({history: snapshot.docs});
      });
  }

  componentWillUnmount() {
    this.unsusbcribe();
  }

  render() {
    return (
      <View>
        {this.state.history.map(doc => {
          <HistoryCard data={doc} />;
        })}
        {this.state.history.length > 0 ? (
          <FlatList
            data={this.state.history}
            renderItem={({item}) => (
              <HistoryCard data={item}>History</HistoryCard>
            )}
          />
        ) : (
          <View>
            <Text>No Data</Text>
          </View>
        )}
      </View>
    );
  }
}
