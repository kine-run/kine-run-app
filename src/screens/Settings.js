import {FlatList, StyleSheet, Text, View, Button} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import React, {Component} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import auth from '@react-native-firebase/auth';

const settingsCategories = [
  {
    id: '1',
    icon: 'account',
    title: 'Profile',
  },
  {
    id: '2',
    icon: 'bell',
    title: 'Notifications',
  },
  {
    id: '3',
    icon: 'web',
    title: 'Language',
  },
];

function Item({icon, title}) {
  return (
    <View style={styles.item}>
      <Icon style={styles.icon} name={icon} />
      <Text style={styles.title}>{title}</Text>
    </View>
  );
}

export default class Settings extends Component {
  onSignOut = async () => {
    await auth().signOut();
    this.props.navigation.navigate('Login');
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <FlatList
          data={settingsCategories}
          renderItem={({item}) => <Item icon={item.icon} title={item.title} />}
          keyExtractor={item => item.id}
        />
        <Button onPress={this.onSignOut} title="Sign Out" />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
    backgroundColor: '#2f2e2d',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    display: 'flex',
    flexDirection: 'row',
  },
  title: {
    color: 'white',
    fontSize: 20,
  },
  icon: {
    color: 'white',
    fontSize: 24,
    marginLeft: 20,
    marginRight: 20,
  },
});
