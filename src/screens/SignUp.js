import {View, StyleSheet, TextInput, Text, Button} from 'react-native';
import React, {Component} from 'react';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

export default class SignUp extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    email: '',
    password: '',
    errorMessage: null,
  };

  static navigationOptions = {
    title: 'Créer un compte',
    headerStyle: {
      backgroundColor: '#211d1c',
    },
    headerTitleStyle: {
      color: 'white',
      textAlign: 'center',
      alignSelf: 'center',
      flex: 1,
    },
  };
  handleSignUp = () => {
    const {email, password} = this.state;
    auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => this.props.navigation.navigate('Race'))
      .catch(error => this.setState({errorMessage: error.message}));

    firestore()
      .collection('users')
      .doc(email)
      .set({
        email: email,
      })
      .catch(error => this.setState({errorMessage: error.message}));
  };

  navigate = () => {
    this.props.navigation.navigate('Login');
  };

  render() {
    return (
      <>
        <View style={styles.globalContainer}>
          <Text style={styles.globalTitle}>Créer un compte</Text>
          <View style={styles.inputContainer}>
            <Text style={styles.title}>E-mail</Text>
            <TextInput
              style={styles.input}
              onChangeText={email => this.setState({email})}
              value={this.state.email}
              autoCapitalize="none"
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.title}>Mot de passe</Text>
            <TextInput
              secureTextEntry={true}
              style={styles.input}
              onChangeText={password => this.setState({password})}
              value={this.state.password}
              autoCapitalize="none"
            />
          </View>
          <View style={styles.button}>
            <Button
              color="#27AE60"
              style={styles.button}
              title="Créer son compte"
              onPress={this.handleSignUp}
            />
          </View>
          {this.state.errorMessage && (
            <Text style={styles.error}>{this.state.errorMessage}</Text>
          )}
          <Text style={styles.title}>Vous avez déjà un compte ?</Text>
          <Button onPress={this.navigate} title="Se connecter" />
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  globalContainer: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#2F2E2D',
    height: '100%',
  },
  globalTitle: {
    color: 'white',
    marginTop: 20,
    fontSize: 18,
  },
  title: {
    color: 'white',
    marginBottom: 10,
    marginTop: 20,
    fontSize: 16,
  },
  inputContainer: {
    marginBottom: 25,
    width: 190,
  },
  input: {
    backgroundColor: 'white',
    borderRadius: 10,
  },
  button: {
    width: 150,
  },
  error: {
    width: 250,
    color: 'red',
  },
});
