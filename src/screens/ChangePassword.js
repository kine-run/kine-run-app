import {View, StyleSheet, TextInput, Text, Button} from 'react-native';
import React, {Component} from 'react';
import auth from '@react-native-firebase/auth';

export default class ChangePassword extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    email: '',
    errorMessage: null,
  };

  static navigationOptions = {
    title: 'Récupération du mot de passe',
    headerStyle: {
      backgroundColor: '#211d1c',
    },
    headerTitleStyle: {
      color: 'white',
      textAlign: 'center',
      alignSelf: 'center',
      flex: 1,
    },
  };

  sendPassword = () => {
    const email = this.state.email;
    auth()
      .sendPasswordResetEmail(email, null)
      .then(() => this.props.navigation.navigate('Login'))
      .catch(error => this.setState({errorMessage: error.message}));
  };

  navigate = () => {
    this.props.navigation.navigate('Login');
  };

  render() {
    return (
      <>
        <View style={styles.globalContainer}>
          <Text style={styles.globalTitle}>Changer de mot de passe</Text>
          <View style={styles.inputContainer}>
            <Text style={styles.title}>E-mail</Text>
            <TextInput
              style={styles.input}
              onChangeText={email => this.setState({email})}
              value={this.state.email}
            />
          </View>
          <View style={styles.button}>
            <Button
              color="#27AE60"
              style={styles.button}
              title="Envoyer un e-mail de récupération"
              onPress={this.sendPassword}
            />
          </View>
          {this.state.errorMessage && (
            <Text style={styles.error}>{this.state.errorMessage}</Text>
          )}
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  globalContainer: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#2F2E2D',
    height: '100%',
  },
  globalTitle: {
    color: 'white',
    marginTop: 20,
    fontSize: 18,
  },
  title: {
    color: 'white',
    marginBottom: 10,
    marginTop: 20,
    fontSize: 16,
  },
  inputContainer: {
    marginBottom: 25,
    width: 190,
  },
  input: {
    backgroundColor: 'white',
    borderRadius: 10,
  },
  button: {
    width: 150,
  },
  error: {
    marginTop: 15,
    width: 250,
    color: 'red',
  },
});
