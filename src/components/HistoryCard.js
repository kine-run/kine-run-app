import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Mapbox, {
  MapView,
  ShapeSource,
  LineLayer,
  Camera,
} from '@react-native-mapbox-gl/maps';
import LinearGradient from 'react-native-linear-gradient';
import TimeFormat from 'hh-mm-ss';

export default class HistoryCard extends Component {
  state = {
    centerCoordinate: [0, 0],
    zoomLevel: 12,
    polyline: {
      type: 'FeatureCollection',
      features: [],
    },
    avgSpeed: 0,
  };

  layerStyle = {
    lineJoin: 'round',
    lineColor: [
      'interpolate',
      ['linear'],
      ['get', 'speed'],
      0,
      '#1abc9c',
      5,
      '#2ecc71',
      10,
      '#3498db',
      15,
      '#9b59b6',
      20,
      '#d35400',
      25,
      '#c0392b',
    ],
    lineWidth: 3,
  };

  styles = StyleSheet.create({
    map: {
      width: '100%',
      height: 200,
      padding: 20,
    },
    overlay: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-evenly',
      alignItems: 'center',
      width: '100%',
      height: 30,
      position: 'absolute',
      top: 0,
    },
    infoGroup: {
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },
    info: {
      color: '#ffffff',
      fontFamily: 'Open Sans',
      fontSize: 15,
    },
  });

  async componentDidMount() {
    this.getData();
  }

  getData = async () => {
    let snapshot = await this.props.data.ref
      .collection('positions')
      .orderBy('timestamp', 'asc')
      .get();
    let polyline = [];
    let total = 0;
    for (let i = 0; i < snapshot.docs.length - 1; i++) {
      let feature = {
        type: 'Feature',
        geometry: {
          type: 'LineString',
          coordinates: [
            [
              snapshot.docs[i].data().coords.longitude,
              snapshot.docs[i].data().coords.latitude,
            ],
            [
              snapshot.docs[i + 1].data().coords.longitude,
              snapshot.docs[i + 1].data().coords.latitude,
            ],
          ],
        },
        properties: {
          altitude: snapshot.docs[i].data().coords.altitude,
          heading: snapshot.docs[i].data().coords.heading,
          speed: snapshot.docs[i].data().coords.speed,
        },
      };
      polyline.push(feature);
      total += snapshot.docs[i].data().coords.speed;
    }
    this.setState({
      polyline: {type: 'FeatureCollection', features: [...polyline]},
      centerCoordinate: polyline[polyline.length - 1].geometry.coordinates[0],
      avgSpeed: total / snapshot.docs.length,
    });
  };

  render() {
    let {zoomLevel, centerCoordinate, polyline, avgSpeed} = this.state;
    let {distance, duration} = this.props.data.data();

    return (
      <MapView
        style={this.styles.map}
        styleURL={Mapbox.StyleURL.Dark}
        zoomEnabled={false}
        scrollEnabled={false}
        pitchEnabled={false}
        rotateEnabled={false}>
        <Camera centerCoordinate={centerCoordinate} zoomLevel={zoomLevel} />
        <LinearGradient
          style={this.styles.overlay}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#27AE60', '#1ABC9C']}>
          <View style={this.styles.infoGroup}>
            <Text style={this.styles.info}>{distance.toFixed(2)} km</Text>
          </View>
          <View style={this.styles.infoGroup}>
            <Text style={this.styles.info}>
              {TimeFormat.fromS(Math.round(duration / 1000), 'hh:mm:ss')}
            </Text>
          </View>
          <View style={this.styles.infoGroup}>
            <Text style={this.styles.info}>{avgSpeed.toFixed(2)} km/h</Text>
          </View>
        </LinearGradient>
        <ShapeSource id="polylineSource" shape={polyline}>
          <LineLayer id="polylineLayer" style={this.layerStyle} />
        </ShapeSource>
      </MapView>
    );
  }
}
