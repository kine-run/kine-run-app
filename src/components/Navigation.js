import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
// import {lightGreen} from '../assets/theme/colors';

import History from '../screens/History';
import Settings from '../screens/Settings';
import Race from '../screens/Race';
import Login from '../screens/Login';
import SignUp from '../screens/SignUp';
import ChangePassword from '../screens/ChangePassword';

const RaceStack = createStackNavigator({
  Race,
});

const LoginStack = createStackNavigator({
  Login,
});

const SignUpStack = createStackNavigator({
  SignUp,
});

const ChangePasswordStack = createStackNavigator({
  ChangePassword,
});

const HistoryStack = createStackNavigator({
  History,
});

const SettingsStack = createStackNavigator({
  Settings,
});

const BottomNavigation = createBottomTabNavigator(
  {
    History: HistoryStack,
    Race: RaceStack,
    Settings: SettingsStack,
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        let iconName;
        if (routeName === 'History') {
          iconName = 'history';
        } else if (routeName === 'Race') {
          iconName = 'run';
        } else if (routeName === 'Settings') {
          iconName = 'settings';
        }

        // You can return any component that you like here!
        return <Icon name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'white',
      inactiveTintColor: 'lightgrey',
      style: {
        backgroundColor: '#1ABC9C',
      },
    },
  },
);

const Navigation = createAppContainer(
  createSwitchNavigator(
    {
      App: BottomNavigation,
      SignUp: SignUpStack,
      Login: LoginStack,
      ChangePassword: ChangePasswordStack,
    },
    {
      initialRouteName: 'Login',
    },
  ),
);

export default Navigation;
