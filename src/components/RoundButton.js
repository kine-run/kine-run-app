import React, {Component} from 'react';
import {StyleSheet, TouchableHighlight} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';

export default class RoundButton extends Component {
  onPress(e) {
    this.props.onPress(e);
  }

  styles = StyleSheet.create({
    button: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      alignContent: 'center',
      alignSelf: 'center',
      width: this.props.size,
      height: this.props.size,
      borderRadius: this.props.size,
      margin: 20,
    },
    gradient: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      alignContent: 'center',
      alignSelf: 'center',
      width: this.props.size,
      height: this.props.size,
      borderRadius: this.props.size,
    },
    icon: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      alignContent: 'center',
      textAlign: 'center',
      alignSelf: 'center',
    },
  });
  render() {
    let {icon, iconSize} = this.props;
    return (
      <TouchableHighlight
        onPress={() => this.onPress(e => this.onPress(e))}
        style={this.styles.button}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#27AE60', '#1ABC9C']}
          style={this.styles.gradient}>
          <Icon
            name={icon}
            size={iconSize}
            color="#fff"
            style={this.styles.icon}
          />
        </LinearGradient>
      </TouchableHighlight>
    );
  }
}
