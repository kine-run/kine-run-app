# KineRun Mobile App

## Introduction

KineRun is a running application available on Android and iOS. It is designed to monitor and keep track of your activity with map visualization and real-time statistics. With it you can record run and save them to your history to watch your progression.

## Installation

Make sure to follow the instruction at [React Native documentation](https://reactnative.dev/docs/getting-started) to install react-native and its dependancies.

#### Android

```bash
npm install
npx react-native run-android
```

#### iOS

```bash
npm install
cd ios && pod install
npx react-native run-ios
```

## Project Architecture

This project use React Native to build native apps on both Android and iOS. We also use Firebase as our Backend for Authentication, Realtime Database and Push Notifications.

![Project Architecture](docs/architecture.png)

## Features

- [x] Navigation (React Navigation)
- [x] Authentication (Firebase Auth)
- [x] Password Reset (Firebase Auth)
- [x] Display Map (MapboxGL)
- [x] Geolocation (MapboxGL)
- [x] Database Storage (Firebase Firestore)
- [x] Display Data on Map (MapboxGL)
- [x] Run History
- [x] Notifications (FCM and Cloud Functions)
- [ ] App Settings
- [ ] Screen lock during run
- [ ] Music Provider and embedded music player
- [ ] Localization (React Native Localize)

## Database structure

Firestore is a NoSQL document-oriented database (similar to MongoDB), so we structured our database with 3 main entities: User, Run and Position.

![Database Structure](docs/database_users.png)
_The user collection is composed of all the registered user of the app, each user got some personnal information and a run collection if he launched at least one run_

![Database Structure](docs/database_runs.png)
_The run collection, we store some information to help calculate and provide some basic statistics to the application. Each run got a collection of position_

![Database Structure](docs/database_positions.png)
_The position collection, the position are the majority of our data. A position is simply a GPS location with it's own data_

## File Structure

```
├── docs              (files used in the documentation)
├── src
│   ├── assets
│   │   ├── fonts
│   │   ├── theme     (const used in our theme)
│   │   ├── images
│   │   └── icons
│   ├── components    (reusable components)
│   ├── screens       (app views)
│   └── App.js
├── .env              (environment variables)
└── README.md         (project documentation)
```

### Website

The _admin_ website was made in order to display some datas and to manage the users by proposing a deleting option.

The website uses the same auth functions as the app does, and we use chartJS in order to display the datas.

Here is the way to install and launch the project:

```bash
npm install
npm run start
```

Now you can access the website through your navigator at `localhost:4200`.

But remember that if you want to create an account, you have to do it in the app !

## Other links

Mobile Application: [https://gitlab.com/kine-run/cloud-function](https://gitlab.com/kine-run/kine-run-app)

Cloud Function: [https://gitlab.com/kine-run/cloud-function](https://gitlab.com/kine-run/cloud-function)

Website: [https://gitlab.com/Kwoak/kine-run-web](https://gitlab.com/Kwoak/kine-run-web)
